import React from "react";
import './ProductItem.css';
import { FaHeart, FaTrashAlt, FaCartPlus  } from 'react-icons/fa';
import {Link, useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";

const ProductItem = ({prop}) => {
    const dispatch = useDispatch()
    const removeProduct = (id) => {
        console.log(id)
        dispatch({type: 'REMOVE_PRODUCT', id})

    }
    const navigate = useNavigate()
    return (
        <div className="box">
            <div className="image">
                <img src={prop.img} alt={'Букет'} onClick={() => navigate('/' + prop.id)}/>
                <div className="icons">
                    <a href=""><FaHeart/></a>
                    <a href="" className="cart-btn" onClick={() => navigate('/' + prop.id)}><FaCartPlus/></a>
                    <Link to='' onClick={() => removeProduct(prop.id)}><FaTrashAlt/></Link>
                </div>
            </div>
            <div className="content">
                <h3>{prop.title}</h3>
                <div className="price">{prop.price}&nbsp;<span>{prop.cost}</span></div>
            </div>
        </div>
    );
}

export default ProductItem;