import React from "react";
import './ProductList.css';
import ProductItem from './ProductItem.js';
import {useSelector} from "react-redux";

const ProductList = () => {
    const product = useSelector(state => state.cards)
    return (
        <div className="box-container">
            {product.map(item =>
                <ProductItem
                    key={item.id}
                    prop={item}
                />)}
        </div>
    );
}

export default ProductList;
