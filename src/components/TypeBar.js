import React from "react";
import "./TypeBar.css";


const TypeBar = () => {
    const category = [
        {id:1, type: 'WOW букеты'},
        {id:2, type: 'Новинки'},
        {id:3, type: 'Самые популярные'},
        {id:4, type: 'Розы'},
        {id:5, type: 'Подарки'}
    ]
    return (
        <div className="category-nav">
            <nav className="nav">
                {category.map(item =>
                    <a key={item.id}>{item.type}</a>
                )}
            </nav>
        </div>

    )
}
export default TypeBar;