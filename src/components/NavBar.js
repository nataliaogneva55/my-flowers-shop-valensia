import React, {Component} from "react";
import './NavBar.css';
import { FaShoppingCart, FaUserAlt, FaBars, FaHeart } from 'react-icons/fa';
import {Link} from "react-router-dom";


class NavBar extends Component {
    render() {
        return (
            <header className='header'>
                <input type="checkbox" id="toggler"/>
                <label htmlFor="toggler">
                    <div className="fa-bars"><FaBars/></div>
                </label>

                <Link to='/' className='logo'>Валенсия<span>.</span></Link>

                <nav className='navbar'>
                    <a href="#home">Главная</a>
                    <a href="#about">О нас</a>
                    <a href="#shop">Магазин</a>
                    <a href="#review">Отзывы</a>
                    <a href="#contact">Контакты</a>
                </nav>
                
                <div className='icons'>
                    <a href="#home"><FaHeart/></a>
                    <a href="#home"><FaShoppingCart/></a>
                    <Link to='/auth'><FaUserAlt/></Link>
                </div>
            </header>
        );
    }
}

export default NavBar;