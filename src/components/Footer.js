import React from "react";
import "./Footer.css";

const Footer = () => {
    return (
        <section className="footer">
            <div className="box-container">
                <div className="box">
                    <h3>навигация</h3>
                    <a href="#">главная</a>
                    <a href="#">о нас</a>
                    <a href="#">магазин</a>
                    <a href="#">отзывы</a>
                    <a href="#">контакты</a>
                </div>

                <div className="box">
                    <h3>полезное</h3>
                    <a href="#">мой аккаунт</a>
                    <a href="#">корзина</a>
                    <a href="#">избранное</a>
                </div>

                <div className="box">
                    <h3>категории</h3>
                    <a href="#">wow букеты</a>
                    <a href="#">новинки</a>
                    <a href="#">самые популярные</a>
                    <a href="#">розы</a>
                    <a href="#">подарки</a>
                </div>

                <div className="box">
                    <h3>свяжитесь с нами</h3>
                    <a href="#">+7(999)-124-24-12</a>
                    <a href="#">valensia@flowers.com</a>
                </div>
            </div>
            <div className="credit"> created by <span> valensia lab </span> | all rights reserved</div>
        </section>
    )

}
export default Footer;