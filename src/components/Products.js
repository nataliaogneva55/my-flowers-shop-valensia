import React from "react";
import TypeBar from "./TypeBar";
import ProductList from "./ProductList/ProductList";


const Products = () => {
    return (
        <div id="shop" className="products">
            <h1 className="heading" > наш <span>магазин</span></h1>
            <TypeBar/>
            <ProductList/>
        </div>
    )
}
export default Products;