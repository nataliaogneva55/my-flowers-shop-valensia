import React, {Component} from "react";
import './Home.css';

let imgUrl = 'https://images.unsplash.com/photo-1602615576820-ea14cf3e476a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80';

class Home extends Component {
    render() {
        return (
            <section className='home' id='home' style={{
                backgroundImage: `url(${imgUrl})`,
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
                backgroundRepeat: 'no-repeat',
            }}>
                <div className='content'>
                    <h3>Свежие цветы</h3>
                    <span>Доставка цветов по России</span>
                    <p>Крупнейшая в России цветочная торгово-производственная компания</p>
                    <a href='#shop' className='btn'>В магазин</a>
                </div>

            </section>
        );
    }
}

export default Home;