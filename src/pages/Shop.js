import React from "react";
import NavBar from "../components/NavBar";
import Home from "../components/Home";
import Products from "../components/Products";
import Footer from "../components/Footer";

const Shop = () => {
    return (
        <>
            <NavBar/>
            <Home/>
            <Products/>
            <Footer/>
        </>
    );
};

export default Shop;