import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import "./Auth.css";
import NavBar from "../components/NavBar";
import img1 from '../assets/img/img-10.jpg';


const Auth = () => {
    const [title, setTitle] = useState('')
    const [subtitle, setSubtitle] = useState('')
    const [desc, setDesc] = useState('')
    const [price, setPrice] = useState('')
    const [cost, setCost] = useState('')
    const [file, setFile] = useState(null)
    const dispatch = useDispatch()
    const selectFile = (e) => {
        setFile(e.target.files[0])
    }

    const addProduct = (e) => {
        e.preventDefault()
        const data = {
            id: Date.now(),
            title: title,
            subtitle: subtitle,
            desc: desc,
            price: `${price}`,
            cost: `${cost}`,
            img: file,
        }
        console.log(data)
        dispatch({type: 'CREATE_PRODUCT', data})
    }

    return (
        <>
            <NavBar/>
            <section className="form-auth">
                <h1 className="heading"><span> Добавление </span> новой карточки </h1>
                <div className="row">
                    <form action="">
                        <input type="text" value={title} className="box" placeholder="Название" onChange={e => setTitle(e.target.value)} />
                        <input type="text" value={subtitle} className="box" placeholder="Тип букета" onChange={e => setSubtitle(e.target.value)}/>
                        <input type="text" value={desc} className="box" placeholder="Описание" onChange={e => setDesc(e.target.value)}/>
                        <input type="text" value={price} className="box" placeholder="Цена" onChange={e => setPrice(Number(e.target.value))}/>
                        <input type="text" value={cost} className="box" placeholder="Старая цена" onChange={e => setCost(Number(e.target.value))}/>
                        <input type="file" className="box" onChange={selectFile} />
                        <button type="submit" className="btn" onClick={addProduct}>Добавить карточку</button>
                    </form>
                    <div className="image">
                        <img src={img1} alt="auth"/>
                    </div>
                </div>
            </section>
        </>
    )
};

export default Auth;