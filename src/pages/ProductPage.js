import React from "react";
import NavBar from "../components/NavBar";
import TypeBar from "../components/TypeBar";
import Footer from "../components/Footer";
import "./ProductPage.css";
import { FaHeart } from 'react-icons/fa';
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";


const ProductPage = () => {
    const param = useParams()
    const id = param.id - 1
    const product = useSelector(state => state.cards[id])
  return (
      <>
          <NavBar/>,
          <section className="card">
              <TypeBar/>

              <h1 className="heading"><span> {product.subtitle} </span> {product.title} </h1>
              <div className="row">

                  <div className="img-container">
                      <img src={product.img} alt="Букет"/>
                  </div>

                  <div className="content">
                      <h3>Оформление заказа</h3>
                      <div className="price"> {product.price} <span>{product.cost}</span></div>
                      <a href="#" className="btn">Добавить в корзину</a>
                      <a href="#" className="heart"><FaHeart/></a>
                      <p>{product.desc}</p>
                  </div>
              </div>
          </section>
          <Footer/>

      </>
  );
};

export default ProductPage;